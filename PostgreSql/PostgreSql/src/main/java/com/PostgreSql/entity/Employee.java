package com.PostgreSql.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Employee {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Pattern(regexp="\\D+",message="name should not contain numbers")
    @Size(min = 3,max = 20)
    private String firstname;

    @Pattern(regexp="\\D+",message="name should not contain numbers")
    @Size(min = 3,max = 20)
    private String lastname;

    @Email(regexp="^(.+)@(.+)$",message="Invalid Email")
    @NotEmpty(message="Email field should not be empty")
    private String email;
}